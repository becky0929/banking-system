package qa.banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {
    public AccountTest(){

    }

    @Test
    public void test_does_balance_match_opening_amount_on_account(){

        //arrange and act
        double openingBalance = 56.78;

        Account acc = new Account(1,"Brian", openingBalance);

        //assert
        assertEquals(openingBalance, acc.availableBalance());
        assertEquals(openingBalance, acc.currentBalance());
    }

    @Test
    public void test_does_balance_match_after_credit(){
        double openingBalance = 56.72;
        double amountToDeposit = 33.45;
        Account acc = new Account(2, "gerald", openingBalance);

        double expectedBalance = acc.credit(amountToDeposit);

        assertEquals(expectedBalance, acc.currentBalance());
        assertEquals(expectedBalance, acc.availableBalance());
    }

    @Test
    public void test_does_balance_match_after_debit(){
        double openingBalance = 56.72;
        double amountToDebit = 33.45;
        Account acc = new Account(2, "gerald", openingBalance);

        double expectedBalance = acc.debit(amountToDebit);

        assertEquals(expectedBalance, acc.currentBalance());
        assertEquals(expectedBalance, acc.availableBalance());
    }
}
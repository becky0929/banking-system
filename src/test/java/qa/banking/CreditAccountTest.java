package qa.banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreditAccountTest {
    public CreditAccountTest(){
    }

    @Test
    public void test_construct_credit_account(){
        double openingBalance = 56.72;
        Account acc = new CreditAccount(1, "brian", openingBalance, InterestRate.BUSINESS_LOAN);

        assertEquals(openingBalance, acc.availableBalance());
        assertEquals(openingBalance, acc.currentBalance());
    }

    @Test
    public void test_going_overdrawn_on_credit_account(){
        double openingBalance = 56.34;
        double amountToDebit = 77.99;
        Account acc = new CreditAccount(1, "Brian", openingBalance, InterestRate.HOME_LOAN);

        acc.debit(amountToDebit);
        double actualResult =openingBalance - (amountToDebit + amountToDebit * InterestRate.HOME_LOAN.rate()/100);

        assertEquals(actualResult, acc.currentBalance());
        assertEquals(actualResult, acc.currentBalance());

    }

    @Test
    public void test_does_balance_match_after_debit_on_credit_account(){

        double openingBalance = 56.34;
        double amountToDebit = 77.99;
        Account acc = new CreditAccount(1, "Brian", openingBalance, InterestRate.HOME_LOAN);

        double expectedBalance = acc.debit(amountToDebit);

        assertEquals(expectedBalance, acc.currentBalance());
        assertEquals(expectedBalance, acc.availableBalance());

    }

}
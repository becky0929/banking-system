package qa.banking;

public enum InterestRate {
    HOME_LOAN(4.32),
    BUSINESS_LOAN(2.67);

    private final double __rate;

    InterestRate(double rate)
    {
        this.__rate = rate;
    }

    public double rate(){
        return __rate;
    }

}

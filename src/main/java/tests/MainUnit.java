package tests;

import qa.banking.Account;
import qa.banking.CreditAccount;
import qa.banking.InterestRate;
import qa.banking.Transaction;

public class MainUnit {
    public static void main(String[] args) {
        test_transactions();
        test_does_balance_match_opening_amount_on_account();
        test_does_balance_match_after_credit();
        test_does_balance_match_after_debit();
        test_construct_credit_account();
        test_going_overdrawn_on_credit_account();
        test_does_balance_match_after_debit_on_credit_account();

    }

    static void test_transactions(){
        Transaction ts = new Transaction(20);
        System.out.println(ts.getAmount());
        System.out.println(ts.getWhen());
    }

    static void test_does_balance_match_opening_amount_on_account() {
        double openingBalance = 56.72;
        Account acc1 = new Account(2, "gerald", openingBalance);

        if (openingBalance == acc1.availableBalance()
                && openingBalance == acc1.currentBalance()) {
            System.out.println("test_does_balance_match_opening_amount_on_account: PASSED");
        } else {
            System.out.println("test_does_balance_match_opening_amount_on_account: FAILED");
        }
    }

    static void test_does_balance_match_after_credit(){
        double openingBalance = 56.72;
        double amountToDeposit = 33.45;
        Account acc = new Account(2, "gerald", openingBalance);

        acc.credit(amountToDeposit);

        if((openingBalance + amountToDeposit) == acc.availableBalance()
                && (openingBalance + amountToDeposit) == acc.currentBalance()){
            System.out.println("test_does_balance_match_after_credit: PASSED");
        }
        else{
        System.out.println("test_does_balance_match_after_credit: FAILED");
    }
}

    static void test_does_balance_match_after_debit(){
        double openingBalance = 56.72;
        double amountToDebit = 33.45;
        Account acc = new Account(2, "gerald", openingBalance);

        acc.debit(amountToDebit);

        if((openingBalance - amountToDebit) == acc.availableBalance()
                && (openingBalance - amountToDebit) == acc.currentBalance()){
            System.out.println("test_does_balance_match_after_debit: PASSED");
        }
        else{
            System.out.println("test_does_balance_match_after_debit: FAILED");
        }
    }

    static void test_construct_credit_account(){
        double openingBalance = 56.72;
        Account acc = new CreditAccount(1, "brian", openingBalance, InterestRate.BUSINESS_LOAN);
        if (openingBalance == acc.availableBalance()
                && openingBalance == acc.currentBalance()) {
            System.out.println("test_construct_credit_account: PASSED");
        } else {
            System.out.println("test_construct_credit_account: FAILED");
        }

    }

    static void test_going_overdrawn_on_credit_account(){

        double openingBalance = 56.34;
        double amountToDebit = 77.99;
        Account acc = new CreditAccount(1, "Brian", openingBalance, InterestRate.HOME_LOAN);

        acc.debit(amountToDebit);

        amountToDebit += amountToDebit * InterestRate.HOME_LOAN.rate()/100;
        if( (openingBalance - amountToDebit) == acc.availableBalance() && (openingBalance -amountToDebit)
        == acc.currentBalance() ){
            System.out.println("test_going_overdrawn_on_credit_account: PASSED");
        }
        else{
            System.out.println("test_going_overdrawn_on_credit_account: FAILED");
        }
    }

    static void test_does_balance_match_after_debit_on_credit_account(){

        double openingBalance = 56.34;
        double amountToDebit = 33.65;
        Account acc = new CreditAccount(1, "Brian", openingBalance, InterestRate.HOME_LOAN);

        acc.debit(amountToDebit);

        //amountToDebit += amountToDebit * InterestRate.HOME_LOAN.rate()/100;
        if( (openingBalance - amountToDebit) == acc.availableBalance() && (openingBalance -amountToDebit
                == acc.currentBalance()) ){
            System.out.println("test_does_balance_match_after_debit_on_credit_account: PASSED");
        }
        else{
            System.out.println("test_does_balance_match_after_debit_on_credit_account: FAILED");
        }
    }

}
